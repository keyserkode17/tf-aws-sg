resource "aws_security_group" "pc_alb_sg" {
  vpc_id      = element(flatten([data.aws_vpcs.vpc.ids]), 0)
  name        = "pcs-alb-sg"
  description = "Allow access"
  ingress {
    from_port       = var.ingressport
    to_port         = var.ingressport
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  #tags        = local.common_tags
}

data "aws_vpcs" "vpc" {
  tags = {
    Application = "aws-infra"
  }
}

data "aws_subnet_ids" "subnets" {
  vpc_id = element(flatten([data.aws_vpcs.vpc.ids]), 0)

  tags = {
    public = false
    tier   =  "webapp"
  }
}

output "sg_id" {
  value = aws_security_group.pc_alb_sg.id
}
